//
// Build HTML
//
// ----------------------------------------------------------------------------

'use strict';

var gulp          = require('gulp-help')(require('gulp'));
var runSequence   = require('run-sequence');
var config        = require('../config.json');
var configPackage = require('../package.json');
var configScampi  = require('../dev/_assets/scampi/package.json');

var twig          = require('gulp-twig');
var twigMarkdown  = require('twig-markdown');
var prettify      = require('gulp-jsbeautifier');



// make:html
// Compile le html
// ----------------------------------------------------------------------------
gulp.task('make:html',"Compile le html", function() {
  return gulp.src(config.paths.pages + '**/*.twig')
    .pipe(twig({
      // errorLogToConsole: true,
      base: config.paths.templates,
      data: {config, configPackage, configScampi},
      extend: twigMarkdown
    }))
    .pipe(gulp.dest(config.paths.build));

});

// make:html-prettify
// Normalise l'indentation du html
// ----------------------------------------------------------------------------
gulp.task('make:html-prettify',"Normalise l'indentation du html", function() {
  gulp.src(config.paths.build + '**/*.html')
    .pipe(prettify({
      indent_size: 2,
      max_preserve_newlines: 1
    }))
//    .pipe(prettify.reporter())
    .pipe(gulp.dest(config.paths.build));
});
