//
// Scripts
//
// ----------------------------------------------------------------------------

'use strict';

var gulp         = require('gulp-help')(require('gulp'));
var runSequence  = require('run-sequence');
var config       = require('../config.json');
var concat       = require('gulp-concat');

// install:js-vendors
// Dépendences javascripts depuis node-modules
// ----------------------------------------------------------------------------
gulp.task('install:js-vendors', "Copie les dépendences js", function() {
  gulp.src(config.paths.assets + 'scampi/js/libs/modernizr.js')
    .pipe(gulp.dest(config.paths.build + '_assets/scripts/vendors'));
  // gulp.src('./node_modules/requirejs/require.js')
  //   .pipe(gulp.dest(config.paths.build + '_assets/scripts/vendors'));
  gulp.src('./node_modules/jquery/dist/jquery.min.js')
    .pipe(gulp.dest(config.paths.build + '_assets/scripts/vendors'));
});


// install:js-scampi
// Copie depuis Scampi les fichiers js vers assets
// Et les concactène dans modules.js
// ----------------------------------------------------------------------------
// gulp.task('install:js-scampi',"Copie depuis Scampi les fichiers js vers assets", function() {
//   gulp.src(config.paths.assets + 'scampi/modules/**/*.js')
//     .pipe(gulp.dest(config.paths.build + '_assets/scripts/scampi/modules'));
//   gulp.src(config.paths.assets + 'scampi/modules/**/*.js')
//     .pipe(concat('modules.js'))
//     .pipe(gulp.dest(config.paths.build + '_assets/scripts/scampi'));
// });
// todo : faire une liste pour construire module.js selon ce qu'on veut


// make:js
// Concaténation des scripts du projet
gulp.task('make:js-main',"Concatène tous les fichiers présents dans dev/projet/scripts/main", function() {
  gulp.src(config.paths.assets + 'project/scripts/main/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest(config.paths.build + '_assets/scripts'))
});
// @todo: le répertoire vide public/scripts/main ne devrait pas être créé :(
