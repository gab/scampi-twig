//
// Build
//
// ----------------------------------------------------------------------------

'use strict';

var gulp         = require('gulp-help')(require('gulp'));
var runSequence  = require('run-sequence');
var config       = require('../config.json');


// build
// Construit le projet et la doc
// ----------------------------------------------------------------------------
gulp.task('build',"Construit le projet", function(callback) {
  runSequence(
    'clean',
    ['install:assets','install:favicon','install:js-vendors','make:js-main','make:css','make:html'],
    ['make:html-prettify'],
    callback);
});


// dev
// build + live
// ----------------------------------------------------------------------------
gulp.task('dev',"build + live", function(callback) {
  runSequence(
    'build',
    'live',
    callback)
});



// install:assets
// Copie les assets dans public
// ----------------------------------------------------------------------------
gulp.task('install:assets',"Copie les assets dans public", function() {
  gulp.src([
    config.paths.assets + 'project/**',
    '!' + config.paths.assets + 'project/scss',
    '!' + config.paths.assets + 'project/**/*.scss',
    '!' + config.paths.assets + 'project/**/*.js'])
    .pipe(gulp.dest(config.paths.build + '_assets'));
});


// install:favicon
// Ajoute la favicon à la racine de public
// ----------------------------------------------------------------------------
gulp.task('install:favicon',"Ajoute la favicon à la racine de public", function() {
  gulp.src(config.paths.assets + 'project/favicon/favicon.ico')
    .pipe(gulp.dest(config.paths.build));
});
